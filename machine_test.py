"""Tests for machine"""
import unittest
import machine


class MachineTest(unittest.TestCase):
    """Machine tests"""

    def test_hello(self):
        output = machine.main(["code/hello_code.txt", "io/hello_input.txt"])
        self.assertEqual(output, "Hello world!")

    def test_cat(self):
        output = machine.main(["code/cat_code.txt", "io/cat_input.txt"])
        with open("io/cat_input.txt", "rt", encoding="utf8") as f:
            sample_output = f.read()
            self.assertEqual(output, sample_output)

    def test_prob1(self):
        output = machine.main(["code/prob1_code.txt", "io/prob1_input.txt"])
        sample_output = str(sum([i for i in range(1, 1000) if i % 3 == 0 or i % 5 == 0]))
        self.assertEqual(output, sample_output)
